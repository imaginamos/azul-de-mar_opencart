<?php
// HTTP
define('HTTP_SERVER', 'http://localhost:8080/2016/open_ecommerce/ecommerce_opencart/admin/');
define('HTTP_CATALOG', 'http://localhost:8080/2016/open_ecommerce/ecommerce_opencart/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost:8080/2016/open_ecommerce/ecommerce_opencart/admin/');
define('HTTPS_CATALOG', 'http://localhost:8080/2016/open_ecommerce/ecommerce_opencart/');

// DIR
define('DIR_APPLICATION', 'C:/xampp/htdocs/2016/open_ecommerce/ecommerce_opencart/admin/');
define('DIR_SYSTEM', 'C:/xampp/htdocs/2016/open_ecommerce/ecommerce_opencart/system/');
define('DIR_IMAGE', 'C:/xampp/htdocs/2016/open_ecommerce/ecommerce_opencart/image/');
define('DIR_LANGUAGE', 'C:/xampp/htdocs/2016/open_ecommerce/ecommerce_opencart/admin/language/');
define('DIR_TEMPLATE', 'C:/xampp/htdocs/2016/open_ecommerce/ecommerce_opencart/admin/view/template/');
define('DIR_CONFIG', 'C:/xampp/htdocs/2016/open_ecommerce/ecommerce_opencart/system/config/');
define('DIR_CACHE', 'C:/xampp/htdocs/2016/open_ecommerce/ecommerce_opencart/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/xampp/htdocs/2016/open_ecommerce/ecommerce_opencart/system/storage/download/');
define('DIR_LOGS', 'C:/xampp/htdocs/2016/open_ecommerce/ecommerce_opencart/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/xampp/htdocs/2016/open_ecommerce/ecommerce_opencart/system/storage/modification/');
define('DIR_UPLOAD', 'C:/xampp/htdocs/2016/open_ecommerce/ecommerce_opencart/system/storage/upload/');
define('DIR_CATALOG', 'C:/xampp/htdocs/2016/open_ecommerce/ecommerce_opencart/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'demo_opencart');
define('DB_PORT', '3306');
define('DB_PREFIX', '');
