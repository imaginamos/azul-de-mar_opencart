<?php
// Heading
$_['heading_title']      = 'Advertisings';

// Text
$_['text_success']       = 'Success: You have modified advertisings!';
$_['text_list']          = 'Advertising List';
$_['text_add']           = 'Add Advertising';
$_['text_edit']          = 'Edit Advertising';
$_['text_default']       = 'Default';

// Column
$_['column_name']        = 'Advertising Name';
$_['column_status']      = 'Status';
$_['column_action']      = 'Action';

// Entry
$_['entry_name']         = 'Advertising Name';
$_['entry_title']        = 'Title';
$_['entry_link']         = 'Link';
$_['entry_image']        = 'Image';
$_['entry_status']       = 'Status';
$_['entry_sort_order']   = 'Sort Order';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify advertisings!';
$_['error_name']         = 'Advertising Name must be between 3 and 64 characters!';
$_['error_title']        = 'Advertising Title must be between 2 and 64 characters!';