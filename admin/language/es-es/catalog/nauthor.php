<?php
// Heading
$_['heading_title']      = 'Noticias / Autores de blog';

// Text
$_['text_success']       = 'Éxito: Se han modificado los autores del blog!';
$_['text_default']       = 'Defecto';
$_['text_image_manager'] = 'Administrador de imágenes';
$_['text_browse']        = 'Browse';
$_['text_clear']         = 'Clear';

// Column
$_['column_name']        = 'Nombre del autor';
$_['column_action']      = 'Acción';

// Entry
$_['entry_name']         = 'Nombre del autor:';
$_['entry_keyword']      = 'SEO Palabra clave:<br /><span class="help">No utilice espacios en lugar sustituir los espacios - y asegúrese de que la palabra clave es único en el mundo</span>';
$_['entry_image']        = 'Avatar:';
$_['entry_type']         = 'Tipo:';
$_['entry_adminid']      = 'Identificador Administrador: <br /><span class="nhelp"> nombre de usuario de la cuenta de administrador - consulte la documentación </span>';
$_['entry_description']  = 'Descripción:';
$_['entry_ctitle']       = 'Título de página personalizado:';
$_['entry_meta_description'] = 'Meta Descripción de la etiqueta:';
$_['entry_meta_keyword'] = 'Etiqueta meta Palabras clave:';
$_['button_insert']      = 'Insertar nuevo Autor';

// Error
$_['error_permission']   = 'Advertencia: Usted no tiene permiso para modificar los autores del blog!';
$_['error_name']         = 'Autor El nombre debe tener entre 3 y 64 caracteres!';
$_['error_product']      = 'Advertencia: Este autor no puede suprimirse, ya que está asignado actualmente a% s artículos!';
$_['error_keyword']      = 'La palabra clave de SEO que ha introducido ya está en uso por alguna otra article/product/category/blog categoria! La palabra clave de SEO debe ser único en la página web.';
?>
