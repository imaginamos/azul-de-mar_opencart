<?php

// Heading
$_['heading_title'] = 'Noticias / Blog Categorías';

// Text
$_['text_success'] = 'Éxito: Ha modificado categorías Noticias / blog!';
$_['text_default'] = 'Defecto';
$_['text_image_manager'] = 'Administrador de imágenes';
$_['text_browse'] = 'Examinar archivos';
$_['text_clear'] = 'Borrar Image';

// Column
$_['column_name'] = 'Nombre de la categoría';
$_['column_sort_order'] = 'Orden de Clasificación';
$_['column_action'] = 'Acción';

// Entry
$_['entry_name'] = 'Nombre de la categoría:';
$_['entry_meta_keyword'] = 'Etiqueta meta Palabras clave:';
$_['entry_meta_description'] = 'Meta Descripción de la etiqueta:';
$_['entry_description'] = 'Descripción:';
$_['entry_parent'] = 'Categoría Padre:';
$_['entry_store'] = 'Tiendas:';
$_['entry_groups'] = 'Visibilidad a grupos de clientes:';
$_['entry_keyword'] = 'SEO Palabra clave:<br/><span class="help">Esto debe ser único global</span>';
$_['entry_image'] = 'Imagen:';
$_['entry_column_display'] = 'Visualización artículos en 2 columnas:<br/><span class="help">Si selecciona, en esta lista de categorías específicas, los artículos se mostrarán en 2 columnas.</span>';
$_['entry_article_limit'] = 'Artículo límite por página:<br/><span class="help"> El número de artículos que se muestran en la página de esta categoría</span>';
$_['entry_sort_order'] = 'Orden de Clasificación:';
$_['entry_status'] = 'Estado:';
$_['entry_layout'] = 'Anulación de diseño:';
$_['button_insert'] = 'Inserte una nueva categoría';

// Error 
$_['error_warning'] = 'Advertencia: Por favor, compruebe cuidadosamente el formulario por errores!';
$_['error_permission'] = 'Advertencia: Usted no tiene permiso para modificar Noticias / Categorías del blog!';
$_['error_name'] = 'Nombre de categoría debe estar entre 2 y 32 caracteres!';
$_['error_keyword'] = 'La palabra clave de SEO que ha introducido ya está en uso por algun otro article/product/category/blog categoria! La palabra clave de SEO debe ser único en la página web.';
?>