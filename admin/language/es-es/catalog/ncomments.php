<?php

// Heading
$_['heading_title'] = 'Noticias Comentarios';

// Text
$_['text_success'] = 'Éxito: Usted tiene comentarios modificados!';

// Column
$_['column_product'] = 'Artículo';
$_['column_author'] = 'Autor';
$_['column_rating'] = 'Clasificación';
$_['column_status'] = 'Estado';
$_['column_date_added'] = 'Fecha de creacion';
$_['column_action'] = 'Accion';
$_['gotonpages'] = 'Ir a Panel de Noticias';

// Entry
$_['entry_product'] = 'Artículo:';
$_['entry_author'] = 'Autor:';
$_['entry_rating'] = 'Clasificación:';
$_['entry_status'] = 'Estado:';
$_['entry_text'] = 'Texto:';
$_['entry_good'] = 'Bueno';
$_['entry_bad'] = 'Malo';
$_['button_insert'] = 'Insertar nuevo comentario';

// Error
$_['error_permission'] = 'Advertencia: Usted no tiene permiso para modificar comentarios de las noticias!';
$_['error_product'] = 'Artículo necesario!';
$_['error_author'] = 'Autor debe estar entre 3 y 64 caracteres!';
$_['error_text'] = 'Comentario de texto debe ser de al menos 1 carácter!';
$_['error_rating'] = 'Evaluacion necesaria!';
?>
