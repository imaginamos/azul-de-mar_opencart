<?php

// Heading
$_['heading_title'] = 'Noticias / Blog Artículos';
$_['heading_titlei'] = 'Insertar / editar el artículo';

// Text
$_['text_success'] = 'Éxito: Se ha modificado el artículo!';
$_['text_thumbnail'] = 'Thumbnail';
$_['text_fullsize'] = 'Tamaño completo';
$_['text_comtot'] = 'Total de comentarios: ';
$_['text_tcaa'] = 'Comentarios a la espera de moderación: ';
$_['text_articles'] = 'Número de artículos agregados: ';
$_['text_commod'] = 'Ir a los comentarios';
$_['text_image_manager'] = 'Administrador de imágenes';
$_['text_browse'] = 'Examinar archivos';
$_['text_clear'] = 'Borrar Imagen';

// Column
$_['column_title'] = 'Encabezado';
$_['column_status'] = 'Estado';
$_['column_action'] = 'Accion';

// Entry
$_['entry_title'] = 'Encabezado:';
$_['tab_category'] = 'Categoría:';
$_['tab_layout'] = 'Anulación de diseño';
$_['tab_general'] = 'General';
$_['tab_seo'] = 'Seo Relacionados';
$_['tab_settings'] = 'Ajustes';
$_['tab_gallery'] = 'Galeria';
$_['entry_acom'] = 'Permitir comentarios para este artículo?';
$_['entry_keyword'] = 'SEO Keyword:';
$_['entry_npages'] = 'Ir al módulo';
$_['entry_description'] = 'Historia principal:';
$_['entry_description2'] = 'Breve descripción <br/><span class="nhelp">aparece en las páginas de la lista en lugar de la breve descripción predeterminada generada a partir de la historia principal.</span>';
$_['entry_addsdesc'] = 'Añadir una breve descripción personalizada';
$_['entry_nauthor'] = 'Autor';
$_['entry_meta_desc'] = 'Meta Descripción de la etiqueta:';
$_['entry_ctitle'] = 'Título de la página personalizada:';
$_['entry_ntags'] = 'Etiquetas: <br /><span class="nhelp">Separados por comas</span>';
$_['entry_meta_key'] = 'Meta Tag keywords:';
$_['entry_status'] = 'Estado:';
$_['entry_groups'] = 'Visibilidad a grupos de clientes::';
$_['entry_image'] = 'Imagen:';
$_['entry_image_size'] = 'Tamaño completo/Thumbnail:';
$_['entry_sort_order'] = 'Orden de Clasificación:';
$_['entry_datea'] = 'Fecha de Creacion:';
$_['entry_dateu'] = 'Fecha de Actualizacion:';
$_['entry_cfield'] = 'Campo de datos personalizados:';
$_['entry_image2'] = 'Foto Destacada: <br /><span class="nhelp">Se mostrará en lugar de la imagen principal del artículo en los listados. Esta imagen no se puede cambiar de tamaño en absoluto.</span>';
$_['entry_related'] = '<b>Productos Relacionados</b><br/>Nombre del Producto: <br /> <span style="color: #666; font-size: 11px;">Esto funciona igual que los productos relacionados en la página del producto. Comience a escribir el nombre del producto y luego seleccionarlo en el menú desplegable.</span>';
$_['entry_nrelated'] = '<b>Articulos Relacionados</b><br/>Nombre del Articulo: <br /> <span style="color: #666; font-size: 11px;">Comience a escribir el nombre del artículo y luego seleccionarlo en el menú desplegable que aparecerá.<span style="color: #f20;">Debe escribir en al menos 3 letras</span></span>';
$_['tab_related'] = 'Relacionado';
$_['tab_custom'] = 'Personalización';
$_['tab_video'] = 'Youtube Videos';
$_['button_remove'] = 'Remover';
$_['button_copy'] = 'Copiar';
$_['button_add_image'] = 'Agregar Imagen';
$_['entry_gallery_text'] = 'Texto de la Imagen';
$_['text_success_delete'] = 'Ha eliminado con éxito los artículos seleccionados';
$_['text_success_copy'] = 'Ha copiado con éxito los artículos seleccionados';
$_['entry_gallery_thumb'] = 'Galería Tamaño del pulgar (W x H): <br /><span class="help">se aplica específicamente al tipo clásico</span>';
$_['entry_gallery_popup'] = 'Tamaño Galería emergente (W x H):';
$_['entry_gallery_slider'] = 'Tamaño Galería deslizante (W x H):';
$_['entry_gallery_slidert'] = 'Tipo de Galeria';
$_['entry_datep'] = 'Fecha de publicación <br /><span class="help">La fecha en que el artículo comenzará a mostrar en el blog.</span>';
$_['entry_video_id'] = 'Youtube video URL';
$_['entry_video_text'] = 'Título (no es obligatorio)';
$_['entry_video_size'] = 'Video Dimenssions (W x H)';
$_['button_add_video'] = 'Agregar Video';
$_['button_insert'] = 'Insertar nuevo artículo';

// Error
$_['error_permission'] = 'Advertencia: Usted no tiene permiso para modificar noticias!';
$_['error_title'] = 'Encabezado debe ser mayor de 3 y menos de 64 caracteres!';
$_['error_description'] = 'Historia principal debe ser mayor de 3 caracteres!';
$_['error_keyword'] = 'La palabra clave de SEO que ha introducido ya está en uso por alguna otra article/product/category/blog category! La palabra clave de SEO debe ser único en la página web.';
?>
