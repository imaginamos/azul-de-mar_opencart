<?php
// Heading
$_['heading_title']     = 'Noticias / Panel Blog';

// Text
$_['text_bwidth']       = 'Ancho: ';
$_['text_bheight']      = 'Alto: ';
$_['text_bnews_image']  = 'Tamaño de la imagen del artículo en la lista de categorías: ';
$_['text_bnews_thumb']  = 'Tamaño de la imagen del artículo en la página del artículo: ';
$_['text_bnews_order']  = 'Orden de los artículos en la categoría listado: ';
$_['text_comtot']       = 'Total de comentarios: ';
$_['text_bysort']       = 'Por orden de clasificación';
$_['text_latest']       = 'Por últimos artículos';
$_['text_bsettings']    = 'Configuración general';
$_['text_tcaa']         = 'Comentarios a la espera de la moderación: ';
$_['text_articles']     = 'Número de artículos insertados: ';
$_['text_commod']       = 'Moderacion de Comentarios';
$_['entry_nmod']        = 'Últimas Noticias módulo';
$_['entry_npages']      = 'Añadir / editar artículos';
$_['entry_ncategory']   = 'Añadir / Modificar categorías';
$_['text_nauthor']      = 'Añadir / editar Autores';
$_['entry_ncmod']       = 'Módulo de artículos de menú';
$_['entry_namod']       = 'Módulo de Archivo Blog';
$_['entry_nbanner']       = 'Añadir / Editar Banner';
$_['text_blogpanel_permissions'] = 'Usted no tiene permisos para modificar las Noticias / Blog';
$_['text_blog_upgrade_success'] = 'Las Noticias / Blog de actualización fue exitosa';
$_['text_blog_install_success'] = 'Las Noticias / Blog La instalación fue exitosa';