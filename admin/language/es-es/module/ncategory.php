<?php

// Heading
$_['heading_title'] = 'Noticias / Blog: Artículos módulo del menú';
$_['title_title'] = 'Módulo de artículos de menú';

// Text
$_['text_module'] = 'Modulos';
$_['text_module_form'] = 'Formulario del módulo del menú';
$_['text_success'] = 'Éxito: Se ha modificado el módulo de los artículos de menú!';
$_['text_content_top'] = 'Contenido Top';
$_['text_content_bottom'] = 'Contenido Bottom';
$_['text_column_left'] = 'Columna Izquierda';
$_['text_column_right'] = 'Columna Derecha';
$_['text_bwidth'] = 'Ancho: ';
$_['text_bheight'] = 'Alto: ';
$_['text_bnews_image'] = 'Tamaño de la imagen del artículo en la lista de categorías: ';
$_['text_bnews_thumb'] = 'Tamaño de la imagen del artículo en la página del artículo: ';
$_['text_bnews_order'] = 'Orden de los artículos en la categoría listado: ';
$_['text_comtot'] = 'Total de comentarios: ';
$_['text_bysort'] = 'Por orden de clasificación';
$_['text_latest'] = 'Por últimos artículos';
$_['text_bsettings'] = 'Ajustes generales de blog';
$_['text_bnews_display_style'] = 'Lista de artículos en:';
$_['text_bnews_dscol'] = '1 Columna';
$_['text_bnews_dscols'] = '2 Columnas';
$_['text_bnews_dselements'] = 'Artículo elementos que aparecen en el listado:';
$_['text_bnews_dse_image'] = 'Imagen';
$_['text_bnews_dse_name'] = 'Nombre';
$_['text_bnews_dse_da'] = 'Fecha de Creacion';
$_['text_bnews_dse_du'] = 'Fecha de Actualizacion';
$_['text_bnews_dse_author'] = 'Autor';
$_['text_bnews_dse_category'] = 'Categorias';
$_['text_bnews_dse_desc'] = 'Descripcion';
$_['text_bnews_dse_button'] = 'Botón Leer Más';
$_['text_bnews_dse_com'] = 'Comentarios';
$_['text_bnews_dse_custom'] = 'Campo personalizado';
$_['text_tplpick'] = 'Cargar custom tpl: <br /><span class="nhelp">consulte la documentación de este.';
$_['text_facebook_tags'] = 'Add facebook meta tags to aricle page head';
$_['text_twitter_tags'] = 'Add twitter meta tags to aricle page head';
$_['tab_disqus'] = 'Disqus Comentarios';
$_['tab_other'] = 'Otras Configuraciones';
$_['tab_disqus_enable'] = 'Habilitar Disqus Comentarios <br /><span class="help">This will disable the built in comment system and you can use disqus instead.';
$_['tab_disqus_sname'] = 'Disqus nombre corto <br /><span class="help">Va a obtener esta información de su cuenta Disqus.';
$_['tab_facebook'] = 'Facebook Comentarios';
$_['tab_facebook_status'] = 'Habilitar Facebook Comentarios <br /><span class="help">This will disable the built in comment system and you can use facebook comments instead.';
$_['tab_facebook_appid'] = 'Facebook App id <br /><span class="help">see documentation</span>';
$_['tab_facebook_theme'] = 'Esquema de la caja, color del comentario';
$_['tab_facebook_posts'] = 'No. de comentarios presentados';
$_['text_bnews_catalog_limit'] = 'Articulos por pagina en el frontend:';
$_['text_bnews_admin_limit'] = 'Articulos por pagina en el administrador:';
$_['text_bnews_headlines_url'] = 'Titulo del blog SEO URL: ';
$_['text_bnews_desc_length'] = 'Auto descripción corta longitud en el listado (número de caracteres):';
$_['text_bnews_restrict_group'] = 'Restringir artículos por grupo de clientes <br /><span style="font-size: 11px; color: #b33;">Observe que al habilitar esta función, artículos y categorías sólo se muestran para los grupos de clientes seleccionados. Si ha actualizado desde una versión anterior del blog y que tienen allready artículos, después de activar esta función, tendrá que editar cada uno de los artículos y seleccionar los grupos de clientes que será visto por.</span>';

// Entry
$_['entry_layout'] = 'Layout:';
$_['entry_position'] = 'Position:';
$_['entry_status'] = 'Status:';
$_['entry_sort_order'] = 'Sort Order:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify the Articles Menu module!';
?>