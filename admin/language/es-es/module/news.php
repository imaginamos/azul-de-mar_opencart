<?php

// Heading
$_['heading_title'] = 'Noticias / Blog: Últimas noticias / artículos por categoría';
$_['title_title'] = 'Últimas noticias / artículos por categoría';

// Text
$_['text_module'] = 'Modulo';
$_['text_success'] = 'Éxito: Ha modificado módulo de novedades!';
$_['text_content_top'] = 'Contenido Superior';
$_['text_content_bottom'] = 'Contenido Abajo';
$_['text_column_left'] = 'Columna izquierda';
$_['text_column_right'] = 'Columna derecha';

// Entry
$_['entry_name'] = 'Nombre del Modulo';
$_['entry_limit'] = 'Limite del Intro';
$_['entry_cat'] = 'Categoria';
$_['entry_nocat'] = 'Lo último de todas las categorías';
$_['entry_npages'] = 'Ir a Panel de Noticias';
$_['entry_status'] = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: Usted no tiene permiso para modificar el módulo últimas noticias!';
$_['error_name'] = 'Nombre del módulo debe ser entre 3 y 64 caracteres!';
?>
