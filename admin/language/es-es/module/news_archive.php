<?php

// Heading
$_['heading_title'] = 'Noticias / Blog: Archivo';
$_['title_title'] = 'Módulo de Archivo Blog';

// Text
$_['text_module'] = 'Modulos';
$_['text_module_form'] = 'Modulo Archivo de Blog';
$_['text_success'] = 'Éxito: Se ha modificado el Módulo Archivo del blog!';
$_['text_content_top'] = 'Contenido Superior';
$_['text_content_bottom'] = 'Contenido Abajo';
$_['text_column_left'] = 'Columna izquierda';
$_['text_column_right'] = 'Columna derecha';
$_['text_jan'] = 'Nombre de Enero';
$_['text_feb'] = 'Nombre de Febrero';
$_['text_march'] = 'Nombre de Marzo';
$_['text_april'] = 'Nombre de Abril';
$_['text_may'] = 'Nombre de Mayo';
$_['text_june'] = 'Nombre de Junio';
$_['text_july'] = 'Nombre de Julio';
$_['text_aug'] = 'Nombre de Agosto';
$_['text_sep'] = 'Nombre de Septiembre';
$_['text_oct'] = 'Nombre de Octubre';
$_['text_nov'] = 'Nombre de Noviembre';
$_['text_dec'] = 'Nombre de Diciembre';
$_['text_build'] = 'Reconstruir archivo del blog automáticamente cuando editando un artículo?';
$_['text_archive_date'] = 'Tipo de fecha del archivo';
$_['text_da'] = 'Articulo fecha de creacion';
$_['text_du'] = 'Articulo fecha de actualizacion';
$_['text_dp'] = 'Articulo fecha de publicacion';


// Entry
$_['entry_status'] = 'Estado:';

// Error
$_['error_permission'] = 'Advertencia: Usted no tiene permiso para modificar el módulo de Archivo Blog!';
