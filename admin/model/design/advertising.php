<?php
class ModelDesignAdvertising extends Model {
	public function addAdvertising($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "advertising SET name = '" . $this->db->escape($data['name']) . "', status = '" . (int)$data['status'] . "'");

		$advertising_id = $this->db->getLastId();

		if (isset($data['advertising_image'])) {
			foreach ($data['advertising_image'] as $advertising_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "advertising_image SET advertising_id = '" . (int)$advertising_id . "', store_id = '" . $this->db->escape($advertising_image['store_id']) . "', link = '" .  $this->db->escape($advertising_image['link']) . "', image = '" .  $this->db->escape($advertising_image['image']) . "', image2 = '" .  $this->db->escape($advertising_image['image2']) . "', sort_order = '0'");

				$advertising_image_id = $this->db->getLastId();

				foreach ($advertising_image['advertising_image_description'] as $language_id => $advertising_image_description) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "advertising_image_description SET advertising_image_id = '" . (int)$advertising_image_id . "', language_id = '" . (int)$language_id . "', advertising_id = '" . (int)$advertising_id . "', title = '" .  $this->db->escape($advertising_image_description['title']) . "', text = '" .  $this->db->escape($advertising_image_description['text']) . "'");
				}
			}
		}

		return $advertising_id;
	}

	public function editAdvertising($advertising_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "advertising SET name = '" . $this->db->escape($data['name']) . "', status = '" . (int)$data['status'] . "' WHERE advertising_id = '" . (int)$advertising_id . "'");
            
		$this->db->query("DELETE FROM " . DB_PREFIX . "advertising_image WHERE advertising_id = '" . (int)$advertising_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "advertising_image_description WHERE advertising_id = '" . (int)$advertising_id . "'");

		if (isset($data['advertising_image'])) {
			foreach ($data['advertising_image'] as $advertising_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "advertising_image SET advertising_id = '" . (int)$advertising_id . "', store_id = '" . $this->db->escape($advertising_image['store_id']) . "', link = '" .  $this->db->escape($advertising_image['link']) . "', image = '" .  $this->db->escape($advertising_image['image']) . "', image2 = '" .  $this->db->escape($advertising_image['image2']) . "', sort_order = '" . (int)$advertising_image['sort_order'] . "'");

				$advertising_image_id = $this->db->getLastId();

				foreach ($advertising_image['advertising_image_description'] as $language_id => $advertising_image_description) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "advertising_image_description SET advertising_image_id = '" . (int)$advertising_image_id . "', language_id = '" . (int)$language_id . "', advertising_id = '" . (int)$advertising_id . "', title = '" .  $this->db->escape($advertising_image_description['title']) . "', text = '" .  $this->db->escape($advertising_image_description['text']) . "'");
				}
			}
		}
	}

	public function deleteAdvertising($advertising_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "advertising WHERE advertising_id = '" . (int)$advertising_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "advertising_image WHERE advertising_id = '" . (int)$advertising_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "advertising_image_description WHERE advertising_id = '" . (int)$advertising_id . "'");
	}

	public function getAdvertising($advertising_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "advertising WHERE advertising_id = '" . (int)$advertising_id . "'");

		return $query->row;
	}

	public function getAdvertisings($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "advertising";

		$sort_data = array(
			'name',
			'status'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getAdvertisingImages($advertising_id) {
		$advertising_image_data = array();

		$advertising_image_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "advertising_image WHERE advertising_id = '" . (int)$advertising_id . "' ORDER BY sort_order ASC");

		foreach ($advertising_image_query->rows as $advertising_image) {
			$advertising_image_description_data = array();

			$advertising_image_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "advertising_image_description WHERE advertising_image_id = '" . (int)$advertising_image['advertising_image_id'] . "' AND advertising_id = '" . (int)$advertising_id . "'");

			foreach ($advertising_image_description_query->rows as $advertising_image_description) {
				$advertising_image_description_data[$advertising_image_description['language_id']] = array('title' => $advertising_image_description['title'], 'text' => $advertising_image_description['text']);
			}

			$advertising_image_data[] = array(
				'advertising_image_description' => $advertising_image_description_data,
				'store_id'                     => $advertising_image['store_id'],
				'link'                     => $advertising_image['link'],
				'image'                    => $advertising_image['image'],
				'image2'                    => $advertising_image['image2'],
				'sort_order'               => $advertising_image['sort_order']
			);
		}

		return $advertising_image_data;
	}

	public function getTotalAdvertisings() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "advertising");

		return $query->row['total'];
	}
}
