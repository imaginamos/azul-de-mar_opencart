<?php

class ControllerModuleAdvertising extends Controller {

    public function index($setting) {
        static $module = 0;
        
        $this->load->model('design/advertising');
        $this->load->model('tool/image');

        $this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.carousel.css');
        $this->document->addScript('catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js');

        $data['advertisings'] = array();
        $data['title'] = $setting['name'];

        $results = $this->model_design_advertising->getAdvertising($setting['advertising_id']);
        $i = 0;
        foreach ($results as $result) {
            if (is_file(DIR_IMAGE . $result['image'])) {
                    $data['advertisings'][] = array(
                        'title' => $result['title'],
                        'text' => $result['text'],
                        'link' => $result['link'],
                        'image' => $this->model_tool_image->resize($result['image'], 240, 377),
                        'image2' => $this->model_tool_image->resize($result['image2'], 636, 420)
                    );
            }
        }

        $data['module'] = $module++;
        
        return $this->load->view('module/advertising', $data);
    }

}
