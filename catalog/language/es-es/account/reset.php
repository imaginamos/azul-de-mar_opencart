<?php
// header
$_['heading_title']  = 'Restablecer contrase&ntilde;a';

// Text
$_['text_account']   = 'Cuenta';
$_['text_password']  = 'Introduzca la nueva contrase&ntilde;a que desea utilizar.';
$_['text_success']   = '&Eacute;xito: La contrase&ntilde;a se ha actualizado correctamente.';

// Entry
$_['entry_password'] = 'Contrase&ntilde;a';
$_['entry_confirm']  = 'Confirmar';

// Error
$_['error_password'] = 'La contrase&ntilde;a debe tener entre 4 y 20 caracteres!';
$_['error_confirm']  = 'Contrase&ntilde;a y confirmación de contrase&ntilde;a no coincide!';
$_['error_code']     = 'C&oacute;digo de restablecimiento de contrase&ntilde;a no es v&aacute;lido o ha sido utilizado anteriormente!';