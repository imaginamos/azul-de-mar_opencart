<?php
// Text
$_['text_captcha'] = 'Captcha';

// Entry
$_['entry_captcha'] = 'Verifica que no eres un robot.';

// Error
$_['error_captcha'] = 'La verificación no es correcta!';
