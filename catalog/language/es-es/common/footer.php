<?php

// Text
$_['text_information'] = 'Informaci&oacute;n';
$_['text_service'] = 'Servicio al Cliente';
$_['text_extra'] = 'Extras';
$_['text_contact'] = 'Contacto';
$_['text_return'] = 'Devoluciones';
$_['text_sitemap'] = 'Mapa';
$_['text_manufacturer'] = 'Marca';
$_['text_voucher'] = 'Vales Regalo';
$_['text_affiliate'] = 'Afiliados';
$_['text_special'] = 'Ofertas';
$_['text_account'] = 'Mi Cuenta';
$_['text_order'] = 'Historial de Pedidos';
$_['text_wishlist'] = 'Lista de Deseos';
$_['text_newsletter'] = 'Bolet&iacute;n Noticias';
$_['text_footer'] = 'Copyright' . date('Y') . ' Todos los derechos reservados.';
$_['text_powered'] = '%s';
$_['text_sign_up_benefits'] = 'Registrese para obtener m&aacute;s beneficios';