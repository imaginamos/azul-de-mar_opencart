<?php

$_['heading_title'] = 'Eventos';

// Text
$_['button_read_more'] = 'Ver mas ...';
$_['text_empty'] = 'No hay eventos';
$_['text_error'] = 'No hay eventos';

/* Month */
$_['january'] = 'Enero';
$_['february'] = 'Febrero';
$_['march'] = 'Marzo';
$_['april'] = 'Abril';
$_['may'] = 'Mayo';
$_['june'] = 'Junio';
$_['july'] = 'Julio';
$_['august'] = 'Agosto';
$_['september'] = 'Septiembre';
$_['october'] = 'Octubre';
$_['november'] = 'Noviembre';
$_['december'] = 'Diciembre';

/* weekDays */
$_['sun'] = 'Dom';
$_['mon'] = 'Lun';
$_['tue'] = 'Mar';
$_['wed'] = 'Mie';
$_['thu'] = 'Jue';
$_['fri'] = 'Vie';
$_['sat'] = 'Sab';
