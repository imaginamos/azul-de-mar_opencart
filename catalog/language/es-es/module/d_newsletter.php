<?php

$_['text_description'] = 'Enter your name and E-mail below to sign up to the Vivasan Health &amp; Beauty Newsletter';
$_['text_success'] = 'Your subscription to the newsletter successfully received!';
$_['text_privacy'] = 'Your information will *never* be shared or sold to a 3rd party.';
$_['entry_firstname'] = 'Enter your first name here';
$_['entry_lastname'] = 'Enter your last name here';
$_['entry_email'] = 'Enter a valid E-mail here';

$_['heading_title'] = 'Unsubscribed';
$_['text_unsubscriber'] = 'You have successfully unsubscribed.';

$_['button_subscribe_popup'] = '★&nbsp;Subscribe';
$_['button_subscribe'] = 'Subscribe';

$_['error_email'] = 'Please enter your E-mail correctly!';

?>