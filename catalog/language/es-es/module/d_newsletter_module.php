<?php

$_['text_description'] = 'Introduzca su nombre y correo electrónico a continuación para inscribirse a nuestro Newsletter';
$_['text_success'] = '¡Se ha suscrito exitosamente a nuestro boletín!';
$_['text_error'] = 'Usted ya se encuentra suscrito a nuestro boletín';
$_['text_privacy'] = '¡Gracias por suscribirte!';
$_['entry_firstname'] = 'Introduzca su nombre aquí';
$_['entry_lastname'] = 'Ingrese su apellido aquí';
$_['entry_email'] = 'Correo electrónico';

$_['button_subscribe_popup'] = '★&nbsp;Subscribe';
$_['button_subscribe'] = 'Suscr&iacute;bete';

$_['error_email'] = 'Por favor, introduzca su dirección de correo correctamente!';

?>