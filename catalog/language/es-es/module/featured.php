<?php

// Heading
$_['text_category'] = 'Categories';
$_['text_manufacturer'] = 'Brand:';
$_['text_model'] = 'Ref:';
$_['text_availability'] = 'Availability:';
$_['text_instock'] = 'In Stock';
$_['text_outstock'] = 'Out Stock';
$_['text_price']        = 'Price: ';
$_['text_tax']          = 'Ex Tax:';
$_['text_quick']          = 'Quick View'; 
$_['button_details']          = 'Details';
$_['reviews']          = 'reviews ';
$_['text_product']          = 'Product {current} of {total} ';
$_['heading_title'] = 'Destacado';
$_['text_sale']      = 'Ofertas';
$_['text_new']      = 'Nuevo';

// Text
$_['text_tax'] = 'Sin Iva:';
