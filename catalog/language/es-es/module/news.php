<?php
// Heading 
$_['heading_title']     = 'Latest Articles';

// Text
$_['text_headlines']    = 'Todos los artículos';
$_['text_comments']     = 'Comnetarios';
$_['text_comments_v']   = 'ver comentarios';
$_['button_more']       = 'Ver más';
$_['text_posted_by']    = 'Autor';
$_['text_posted_on']    = 'En';
$_['text_posted_pon']   = 'Publicado el';
$_['text_posted_in']    = 'Añadido';
$_['text_updated_on']   = 'Actualizado el';
$_['text_blogpage']     = 'Blog';
?>
