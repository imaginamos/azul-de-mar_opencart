<?php
// Text
$_['text_error']	    = 'No hay ningún artículo aquí!';
$_['text_read_more']    = 'Ver más';
$_['entry_name']        = 'Nombre:';
$_['entry_comment']     = 'Comentario:';
$_['news_prelated']     = 'Productos relacionados con este artículo';
$_['news_related']      = 'Artículos relacionados';
$_['nocomment']         = 'No comments';
$_['writec']            = 'Escribir comentario';
$_['title_comments']    = 'Comentarios Para';
$_['bsend']             = 'Comentar';
$_['entry_captcha']     = 'Enter the code in the box below:';
$_['text_note']         = ' '; //<span style="color: #FF0000;">Note:</span> HTML is not translated!
$_['text_wait']         = 'Espere por favor';
$_['text_success']      = 'Gracias por tu comentario. Se ha presentado al administrador del sitio para su aprobación.';
$_['text_posted_in']    = 'Añadido en';
$_['text_updated_on']   = 'Actualizado el';	
$_['text_tags']         = 'Tags:';	
$_['text_comments']     = 'Comentarios sobre este artículo';
$_['text_comments_v']   = 'Ver comentarios';
$_['text_comments_to']  = 'Comentarios a';
$_['text_tax']          = 'Ex Tax:';
$_['button_more']       = 'Ver m&aacute;s';
$_['text_posted_by']    = 'Publicado por';
$_['text_posted_on']    = 'En';
$_['text_posted_pon']   = 'Publicado el';
$_['text_reply_to']     = 'Responder a';
$_['text_reply']        = 'Responder este comentario';
$_['author_text']       = 'Sobre el Autor';

// Buttons
$_['button_news']       = 'Blog´s';

//Errors
$_['error_name']        = 'Advertencia: Comentarios del autor debe estar entre 3 y 25 caracteres!';
$_['error_text']        = 'Advertencia: Comentario de texto debe estar entre los 25 y los 1000 caracteres!';
$_['error_captcha']     = 'Advertencia: Código de verificación no coincide con la imagen!';
?>
