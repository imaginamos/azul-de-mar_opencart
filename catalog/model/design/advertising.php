<?php
class ModelDesignAdvertising extends Model {
	public function getAdvertising($advertising_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "advertising_image bi LEFT JOIN " . DB_PREFIX . "advertising_image_description bid ON (bi.advertising_image_id  = bid.advertising_image_id) WHERE bi.advertising_id = '" . (int)$advertising_id . "' AND bid.language_id = '" . (int)$this->config->get('config_language_id') . "' AND store_id = '".(int)$this->config->get('config_store_id')."' ORDER BY bi.sort_order ASC");

		return $query->rows;
	}
}