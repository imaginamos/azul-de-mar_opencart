//* Autor: Esteban Vera
// Twitter: @kiokotzu
// Email:esteban.vg@outlook.com, esteban.vera@imagina.co */



/*------------------------------------*\
    $DEPENDECIAS
\*------------------------------------*/

var gulp        = require('../../../../../../node_modules/gulp'),
    stylus      = require('../../../../../../node_modules/gulp-stylus'),
    nib         = require('../../../../../../node_modules/nib'),
    plumber     = require('../../../../../../node_modules/gulp-plumber'),
    concat      = require('../../../../../../node_modules/gulp-concat'),
    watch       = require('../../../../../../node_modules/gulp-watch'),
    uglify      = require('../../../../../../node_modules/gulp-uglify'),
    notify      = require('../../../../../../node_modules/gulp-notify'),
    jshint      = require('../../../../../../node_modules/gulp-jshint');
    // imagemin    = require('../../../../../../node_modules/gulp-imagemin'),
    // pngquant    = require('../../../../../../node_modules/imagemin-pngquant');





/*------------------------------------*\
    $RUTAS
\*------------------------------------*/

var src_stylus = "stylus/moduplak.styl",
    //importante tener cuidado con el orden de este array, posee las rutas de los archivos .js, esto implica no tener conflictos entre librerias
    // src_js     = ['js/lib/jquery-1.11.0.min.js','js/lib/**/*.js', 'js/ajaxluis.js', 'js/main.js', 'js/app.js','js/chat/**/*.js'],
    // src_js     = ['js/lib/jquery-1.11.0.min.js','js/lib/owl.carousel.min.js', 'js/lib/jquery.magnific-popup.js', 'js/lib/jquery.scrollpanel.js', 'js/app.js'],
    src_imgs   = "imgs/*.*",
    dest_css   = "css/",
    dest_js    = "js/",    
    dest_imgs  = "img/";





/*------------------------------------*\
    $LINEA ERROR
\*------------------------------------*/
var linea = function(e){
  console.log(e);
  this.emit('termino');
}




/*----------------------------------------------------------------------------------------*\
    $TAREAS
\*----------------------------------------------------------------------------------------*/


/*------------------------------------*\
    $STYLUS
\*------------------------------------*/
gulp.task('stylus', function () {

  return gulp.src(src_stylus)
    .pipe(plumber({
      errorHandler : linea
    }))
    .pipe(stylus({
      compress: true,
      use: [nib()]
    }))
    .pipe(gulp.dest(dest_css))
    .pipe(notify("Compilo Stylus"));

});


/*------------------------------------*\
    $JAVASCRIPT
\*------------------------------------*/

gulp.task('js',function(){

  return gulp.src(src_js)
    .pipe(plumber({
      errorHandler : linea
    }))
    .pipe(jshint())
    // .pipe(jshint.reporter('default'))
    .pipe(uglify())
    .pipe(concat('app.min.js'))
    .pipe(gulp.dest(dest_js))
    .pipe(notify("Compilo JS"));

});


/*------------------------------------*\
    $IMAGENES
\*------------------------------------*/

// gulp.task('img', function () {
//     return gulp.src([src_imgs])
//         .pipe(imagemin({
//             progressive: false,
//             optimizationLevel : 6,
//             svgoPlugins: [{removeViewBox: false}],
//             use: [pngquant()]
//         }))
//         .pipe(gulp.dest(dest_imgs))
//         .pipe(notify("Compilo Imagenes"));
// });











/*------------------------------------*\
    $AUTO COMPILAR
\*------------------------------------*/

gulp.task('watch',function(){
  gulp.watch(src_stylus,['stylus']);
  // gulp.watch(src_js,['js']);
});







/*------------------------------------*\
    $POR DEFECTO
\*------------------------------------*/

gulp.task('default',['watch','stylus']);