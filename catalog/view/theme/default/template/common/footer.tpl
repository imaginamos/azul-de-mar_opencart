<a href="javascript:void(0);" class="top_up"><i class="fa fa-angle-up"></i></a>
<footer>
    <div class="container">
        <div class="row text-center">
            <div class="col-md-4 col-xs-12 mb30">
                <h2>CONTACTO</h2>
                <a href="mailto:info@azuldemar.com" class="email">info@azuldemar.com</a>
            </div>
            <div class="col-md-4 col-xs-12 mb30">
                <h2>SÍGUENOS EN</h2>
                <div class="social">
                    <?php if(!empty($facebook)): ?>
                    <a target="_blank" href="<?php echo $facebook; ?>" class="facebook"><i class="fa fa-facebook"></i></a>
                    <?php endif; ?>
                    <?php if(!empty($twiter)): ?>
                    <a target="_blank" href="<?php echo $twiter; ?>" class="twitter"><i class="fa fa-twitter"></i></a>
                    <?php endif; ?>
                    <?php if(!empty($instagram)): ?>
                    <a target="_blank" href="<?php echo $instagram; ?>" class="instagram"><i class="fa fa-instagram"></i></a>
                    <?php endif; ?>
                    <?php if(!empty($pinterest)): ?>
                    <a target="_blank" href="<?php echo $pinterest; ?>" class="instagram"><i class="fa fa-pinterest-p"></i></a>
                    <?php endif; ?>
                </div>
                <div class="clear"></div>
            </div>
            <div class="col-md-4 col-xs-12 mb30">
                <h2>NEWSLETTER</h2>
                <form method="get" class="crud form_newsletter ng-pristine ng-valid" id="form_newsletter">
                    <div class="inline_table">
                        <input type="text" name="email" id="email" value="" class="form-control" placeholder="Email">
                        <button type="submit" name="btnAction" class="btn btn-primary btn-xs">Subscríbete</button>
                    </div>
                </form>
            </div>
            <div class="clear"></div>
        </div>
        <div class="row text-center">    
            <?php if ($informations) { ?>
            <div class="col-sm-12 mt20">
                <ul class="list-unstyled">
                   <?php foreach ($informations as $information) { ?>
                    <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                    <?php } ?>
                    <!-- <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li> -->
                    <!-- <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li> -->
                    <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
                    <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li> 
                </ul>
            </div>
            <?php } ?>
            <div class="col-sm-12 mt30 mb10">
                
            </div>
          
        </div>
    </div>
    <div class="copyright hide">
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <p>
                        <span class="rights">Copyright © <?php echo date("Y"); ?> <?php echo $powered; ?>. Todos los derechos reservados.</span>
                    </p>
                </div>
                <div class="col-sm-3">
                    <a href="http://imaginamos.com/" class="imaginamos">Diseño web imagin<span>a</span>mos.com</a>
                </div>
            </div>
        </div>
    </div>
</footer>

</body>
</html>