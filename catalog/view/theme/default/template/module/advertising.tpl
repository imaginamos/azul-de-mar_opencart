<?php if(isset($advertisings)) { ?>
<section class="box-primary-home">

    <?php foreach ($advertisings as $advertising) { ?>
    <div class="row no_padding">
        <div class="box-content">
            <div class="col-lg-6 col-md-6 col-xs-12">
                <div class="box-padd">
                    <div class="box-description">
                        <div class="cont-text">
                            <h2><?php echo $advertising['title']; ?></h2>
                            <div class="text"><?php echo $advertising['text']; ?></div>
                            <a href="<?= $advertising['link'] ?>" class="link">Ver m&aacute;s</a>
                        </div>
                        <div class="cont-img">
                            <a href="<?= $advertising['link'] ?>">
                                <img src="<?php echo $advertising['image']; ?>" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-xs-12">
                <a href="<?= $advertising['link'] ?>">
                    <div class="box-padd" style="background-image: url('<?php echo $advertising['image2']; ?>');">
                        <div class="box-hover-description hide">
                            <h2><?php echo $advertising['title']; ?></h2>
                            <div class="text"><?php echo $advertising['text']; ?></div>
                            <!-- <a href="<?= $advertising['link'] ?>" class="link">Ver Más</a> -->
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <?php } ?>

</section>
<?php } ?>