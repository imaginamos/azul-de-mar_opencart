<div id="carousel<?php echo $module; ?>" class="owl-carousel">
  <?php foreach ($banners as $banner) { ?>
  <div class="item text-center">
    <?php if ($banner['link']) { ?>
    <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" /></a>
    <?php } else { ?>
    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
    <?php } ?>
  </div>
  <?php } ?>
</div>
<script type="text/javascript"><!--
$('#carousel<?php echo $module; ?>').owlCarousel({
  loop: ($('#carousel<?php echo $module; ?> .item').length > 4) ? true : false,
  nav:($('#carousel<?php echo $module; ?> .item').length > 4) ? true : false,
  margin: 0,
  autoplay: true,
  autoplayTimeout: 5000,
  animateOut: 'fadeOut',
  animateIn: 'fadeIn',
  dots: false,
  margin: 0,
  smartSpeed: 1000,
  pagination: false,
  navText: [],
  responsive: {
    0: {
      items: 1
    },
    380: {
      items: 2
    },
    620: {
      items: 3
    },
    850: {
      items: 4
    }
  }
});
--></script>