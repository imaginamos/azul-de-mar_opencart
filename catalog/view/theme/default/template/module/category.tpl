<h2>Categorías</h2>
<ul class="list-group accordeon-menu">
  <?php foreach ($categories as $category) { ?>
  <?php if ($category['category_id'] == $category_id) { ?>
  <li class="<?php echo ($category['category_id'] == $category_id ? 'active': '')?>">
    <a href="<?php echo $category['href']; ?>" class="list-group-item"><?php echo $category['name']; ?></a>
    <?php if ($category['children']) { ?>
    <span class="submenu-indicator">+</span>
    <ul class="submenu">
    <?php foreach ($category['children'] as $child) { ?>
      <li class="<?php echo ($child['category_id'] == $child_id ? 'active': '')?>"><a href="<?php echo $child['href']; ?>" class="list-group-item "><?php echo $child['name']; ?></a>
      <?php if ($child['children']) { ?>
        <span class="submenu-indicator">+</span>
        <ul class="submenu">
        <?php foreach ($child['children'] as $child2) { ?>
        <li class="<?php echo ($child2['category_id'] == $child_id ? 'active': '')?>"><a href="<?php echo $child2['href']; ?>" class="list-group-item "><?php echo $child2['name']; ?></a></li>
        <?php } ?>
        </ul>
      <?php } ?>
      </li>
    <?php } ?>
    </ul>
    <?php } ?>
  </li>
  <?php } else { ?>
  <li><a href="<?php echo $category['href']; ?>" class="list-group-item"><?php echo $category['name']; ?></a></li>
  <?php } ?>
  <?php } ?>
</ul>