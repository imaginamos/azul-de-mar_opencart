<div id="slideshow<?php echo $module; ?>" class="owl-carousel" style="opacity: 1;">
  <?php foreach ($banners as $banner) { ?>
  <div class="item">
    <?php if ($banner['link']) { ?>
    <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" /></a>
    <?php } else { ?>
    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
    <?php } ?>
  </div>
  <?php } ?>
</div>
<script type="text/javascript"><!--
$('#slideshow<?php echo $module; ?>').owlCarousel({
  items: 1,
  loop: ($('#slideshow<?php echo $module; ?> .item').length > 1) ? true : false,
  margin: 0,
  nav: ($('#slideshow<?php echo $module; ?> .item').length > 1) ? true : false,
  autoplay: ($('#slideshow<?php echo $module; ?> .item').length > 1) ? true : false,
  dots: false,
  autoplayTimeout: 5000,
  animateOut: 'fadeOut',
  animateIn: 'fadeIn',
  smartSpeed: 800,
  navText: [],
  pagination: false
});
--></script>