<?php
//VARS
define('ROUTER_PATH', $_SERVER['DOCUMENT_ROOT'] . '/moduplack_opencart/');
define('ROUTER_URL', $_SERVER['HTTP_HOST'] . '/moduplack_opencart/');
// HTTP
define('HTTP_SERVER', 'http://' . ROUTER_URL);

// HTTPS
define('HTTPS_SERVER', 'https://' . ROUTER_URL);

// DIR
define('DIR_APPLICATION', ROUTER_PATH . 'catalog/');
define('DIR_SYSTEM', ROUTER_PATH . 'system/');
define('DIR_IMAGE', ROUTER_PATH . 'image/');
define('DIR_LANGUAGE', ROUTER_PATH . 'catalog/language/');
define('DIR_TEMPLATE', ROUTER_PATH . 'catalog/view/theme/');
define('DIR_CONFIG', ROUTER_PATH . 'system/config/');
define('DIR_CACHE', ROUTER_PATH . 'system/storage/cache/');
define('DIR_DOWNLOAD', ROUTER_PATH . 'system/storage/download/');
define('DIR_LOGS', ROUTER_PATH . 'system/storage/logs/');
define('DIR_MODIFICATION', ROUTER_PATH . 'system/storage/modification/');
define('DIR_UPLOAD', ROUTER_PATH . 'system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'demo_opencart');
define('DB_PORT', '3306');
define('DB_PREFIX', '');